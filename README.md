# ANALIZADOR LÉXICO DE PYTHON EN C #

Construcción de un analizador léxico que devuelve los componentes léxicos que aparecen en un programa python

### Estructura del proyecto ###

* Este proyecto cuenta con una arquitectura y orden de ejecución claramente definida, un fichero definiciones.h donde se encuentran las definiciones de las palabras reservadas, entrada.c y .h que utilizo para la lectura del fichero .py, errores.c para la gestión de errores, lexico.c que implementa un autómata finito para la detección de cadenas alfanuméricas, sus funciones se hacen públicas gracias a la ayuda de su correspondiente archivo de cabecera .h, el analizador sintáctico que toma la iniciativa de pedir al analizador léxico el siguiente componente léxico, y unos ficheros asociados a las funcionalidades de la tabla de símbolos (que en este caso desarrollo con una tabla hash), a parte de un archivo .py de prueba.
* Inicialización de tabla de símbolos funcional

### Tecnologías necesarias para la ejecución ###

* Sistema operativo de desarrollo: Ubuntu 16.04 LTS
* Se aportará un makefile para la compilación del programa

