#include <stdio.h>
#include <stdlib.h>

char* leer(void) {

	char* lista_caracteres = (char *) malloc(1923);

	/* Declaramos la variable fichero como puntero a FILE. */
	FILE *fichero;

	/* Declaramos la variable caracter de tipo char. */
	char caracter;

	/* Abrimos "fichero1.txt" en modo texto y
	 * guardamos su direccion en el puntero. */
	fichero = fopen("wilcoxon.py", "rt");

	if (fichero == NULL) {
		printf("Error: No se ha podido crear el fichero fichero1.txt");
	} else {
		/* Se obtiene el primer caracter del fichero
		 * y se almacena en la variable caracter. */
		int i = 0;
		while (feof(fichero) == 0) {
			lista_caracteres[i] = fgetc(fichero);
			i+=1;
		}
		/* Cerramos fichero */
		fclose(fichero);
	}

	return lista_caracteres;
}
