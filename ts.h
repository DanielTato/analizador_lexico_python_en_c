#include <stdio.h>
#include <stdlib.h>
#include <string.h>


typedef struct hash_node
{
	struct hash_node *next;	
	char *key;				
	void *value;			
	char is_occupyed;	
}Hash_node;

typedef struct hash_table
{
	Hash_node **table;	
}Hash_Table;

typedef struct commodity
{
	char name[32];	
	int price;	
}Com;


static unsigned int JSHash(char* key, unsigned int key_len);

static void init_hs_node(Hash_node *node);

static Hash_Table *creat_hash_table(void);

int add_node2HashTable(Hash_Table *Hs_table, char *key, unsigned int key_len, void *value);

void *get_value_from_hstable(Hash_Table *Hs_table, char *key, unsigned int key_len);

void hash_table_delete(Hash_Table *Hs_Table);

void printf_com_info(Com *com);

int inicializar(int argc, char **argv);
